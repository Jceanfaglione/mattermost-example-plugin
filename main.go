package main

import (
	"fmt"
	"net/http"

	"github.com/mattermost/mattermost-server/v5/plugin"
)

// SimplePlugin implements the interface expected by the Mattermost server to communicate
// between the server and plugin processes.
type SimplePlugin struct {
	plugin.MattermostPlugin
}

// ServeHTTP demonstrates a plugin that handles HTTP requests by greeting the world.
func (p *SimplePlugin) ServeHTTP(c *plugin.Context, w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Simple Mattermost Plugin.  Jon C.")
}

// This example demonstrates a plugin that handles HTTP requests which respond by greeting the
// world.
func main() {
	plugin.ClientMain(&SimplePlugin{})
}

// sample function to test linter
func Foo(bar int) int {
	if bar > 0 {
		return 123
	} else {
		return 456
	}
}
