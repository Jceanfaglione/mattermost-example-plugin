FROM golang:1.13.3-stretch AS builder

ENV GO111MODULE=on \
    CGO_ENABLED=0

RUN apt-get update && apt-get install -y git

WORKDIR $GOPATH/src/repo1.dsop.io/mattermost-simple-plugin/

COPY . .

RUN go get -u golang.org/x/lint/golint
RUN golint $(go list ./... | grep -v /vendor/)

RUN go get -u github.com/mattermost/mattermost-server

# Build the binary.
RUN go build -o bin/mm-plugin main.go

RUN ls -al $GOPATH/src/repo1.dsop.io/mattermost-simple-plugin/

FROM registry.access.redhat.com/ubi8/ubi-minimal:8.1

WORKDIR /app

COPY --chown=0:0 --from=builder /go/src/repo1.dsop.io/mattermost-simple-plugin/bin/mm-plugin /app/

CMD ["./mm-plugin"]