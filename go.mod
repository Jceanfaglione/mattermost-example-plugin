module dsop.io/platform-one/plugins/mattermost/plugin-example

go 1.13

require (
	github.com/mattermost/mattermost-server v5.11.1+incompatible // indirect
	github.com/mattermost/mattermost-server/v5 v5.23.0
)
